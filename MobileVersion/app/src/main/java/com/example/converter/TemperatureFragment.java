package com.example.converter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class TemperatureFragment extends Fragment {

    private String[] velichini = {"C to K", "C to F", "C to Re", "C to Ro", "C to Ra", "C to N", "C to D", "K to C", "F to C", "Re to C", "Ro to C", "Ra to C", "N to C", "D to C"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.converter_screen, container, false);

        ArrayAdapter<String> firstValueSpinnerAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, velichini);
        firstValueSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerFirstValue);
        spinner.setAdapter(firstValueSpinnerAdapter);

        final EditText firstValueToConvert = (EditText) rootView.findViewById(R.id.editText1);
        final TextView result = (TextView) rootView.findViewById(R.id.editText2);

        Button convertButton = (Button) rootView.findViewById(R.id.button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conversion = spinner.getSelectedItem().toString();
                String value = firstValueToConvert.getText().toString();
                result.setText(temperatureWorker(value, conversion));
            }
        });

        return rootView;
    }

    public static String temperatureWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }

        switch (convertation) {
            case "C to K":
                result = Double.toString(value + 273.15);
                break;
            case "C to F":
                result = Double.toString(value * (9 / 5) + 32);
                break;
            case "C to Re":
                result = Double.toString(value * (4 / 5));
                break;
            case "C to Ro":
                result = Double.toString(value * (21 / 40) + 7.5);
                break;
            case "C to Ra":
                result = Double.toString((value + 273.15) * (9 / 5));
                break;
            case "C to N":
                result = Double.toString(value * 0.33);
                break;
            case "C to D":
                result = Double.toString((100 - value) * (3 / 2));
                break;
            case "K to C":
                result = Double.toString(value - 273.15);
                break;
            case "F to C":
                result = Double.toString((value - 32) * (5 / 9));
                break;
            case "Re to C":
                result = Double.toString(value * (5 / 4));
                break;
            case "Ro to C":
                result = Double.toString((value - 7.5) * (40 / 21));
                break;
            case "Ra to C":
                result = Double.toString((value - 491.67) * (5 / 9));
                break;
            case "N to C":
                result = Double.toString(value * (100 / 33));
                break;
            case "D to C":
                result = Double.toString((100 - value) * (2 / 3));
                break;
            default:
                result = "Choose convertation";
        }
        return result;

    }

}
