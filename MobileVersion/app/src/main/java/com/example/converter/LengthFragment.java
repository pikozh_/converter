package com.example.converter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class LengthFragment extends Fragment {

    private String[] velichini = {"m to km", "m to mile", "m to nautical mile", "m to cable", "m to league", "m to foot", "m to yard", "km to m", "mile to m", "nautical mile to m", "cable to m", "league to m", "foot to m", "yard to m"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.converter_screen, container, false);

        ArrayAdapter<String> firstValueSpinnerAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, velichini);
        firstValueSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerFirstValue);
        spinner.setAdapter(firstValueSpinnerAdapter);

        final EditText firstValueToConvert = (EditText) rootView.findViewById(R.id.editText1);
        final TextView result = (TextView) rootView.findViewById(R.id.editText2);

        Button convertButton = (Button) rootView.findViewById(R.id.button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conversion = spinner.getSelectedItem().toString();
                String value = firstValueToConvert.getText().toString();
                result.setText(lengthWorker(value, conversion));
            }
        });

        return rootView;
    }

    public static String lengthWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }

        switch (convertation) {
            case "m to km":
                result = Double.toString(value * 0.001);
                break;
            case "m to mile":
                result = Double.toString(value * 0.000621371192);
                break;
            case "m to nautical mile":
                result = Double.toString(value * 0.000539956803);
                break;
            case "m to cable":
                result = Double.toString(value * 0.00455672208);
                break;
            case "m to league":
                result = Double.toString(value * 0.000179985601);
                break;
            case "m to foot":
                result = Double.toString(value * 3.2808399);
                break;
            case "m to yard":
                result = Double.toString(value * 1.0936133);
                break;
            case "km to m":
                result = Double.toString(value / 0.001);
                break;
            case "mile to m":
                result = Double.toString(value / 0.000621371192);
                break;
            case "nautical mile to m":
                result = Double.toString(value / 0.000539956803);
                break;
            case "cable to m":
                result = Double.toString(value / 0.00455672208);
                break;
            case "league to m":
                result = Double.toString(value / 0.000179985601);
                break;
            case "foot to m":
                result = Double.toString(value / 3.2808399);
                break;
            case "yard to m":
                result = Double.toString(value / 1.0936133);
                break;
            default:
                result = "Choose convertation";
        }
        return result;

    }
}
