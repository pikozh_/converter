package com.example.converter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


public class WeightFragment extends Fragment {
    private String[] velichini = {"kg to g", "kg to c", "kg to carat", "kg to eng pound", "kg to pound", "kg to stone", "g to kg", "c to kg", "carat to kg", "eng pound to kg", "pound to kg", "stone to kg"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.converter_screen, container, false);

        ArrayAdapter<String> firstValueSpinnerAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, velichini);
        firstValueSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerFirstValue);
        spinner.setAdapter(firstValueSpinnerAdapter);

        final EditText firstValueToConvert = (EditText) rootView.findViewById(R.id.editText1);
        final TextView result = (TextView) rootView.findViewById(R.id.editText2);
        Button convertButton = (Button) rootView.findViewById(R.id.button);

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conversion = spinner.getSelectedItem().toString();
                String value = firstValueToConvert.getText().toString();
                result.setText(weightWorker(value, conversion));
            }
        });

        return rootView;
    }

    public static String weightWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }

        switch (convertation) {
            case "kg to g":
                result = Double.toString(value * 1000);
                break;
            case "kg to c":
                result = Double.toString(value * 100);
                break;
            case "kg to carat":
                result = Double.toString(value * 5000);
                break;
            case "kg to eng pound":
                result = Double.toString(value / 0.45359237);
                break;
            case "kg to pound":
                result = Double.toString(value * 0.40951241);
                break;
            case "kg to stone":
                result = Double.toString(value * 0.157473044);
                break;
            case "g to kg":
                result = Double.toString(value / 1000);
                break;
            case "c to kg":
                result = Double.toString(value / 100);
                break;
            case "carat to kg":
                result = Double.toString(value / 5000);
                break;
            case "eng pound to kg":
                result = Double.toString(value * 0.45359237);
                break;
            case "pound to kg":
                result = Double.toString(value / 0.40951241);
                break;
            case "stone to kg":
                result = Double.toString(value / 0.157473044);
                break;
            default:
                result = "Choose convertation";
        }
        return result;

    }
}
