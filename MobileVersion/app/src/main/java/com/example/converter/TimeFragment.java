package com.example.converter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class TimeFragment extends Fragment {

    private String[] velichini = {"Seconds to Minutes", "Seconds to Hour", "Seconds to Day", "Seconds to Week", "Seconds to Month", "Seconds to Astronomical Year", "Seconds to Third", "Minutes to Seconds", "Hour to Seconds", "Day to Seconds", "Week to Seconds", "Month to Seconds", "Astronomical Year to Seconds", "Third to Seconds"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.converter_screen, container, false);

        ArrayAdapter<String> firstValueSpinnerAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, velichini);
        firstValueSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerFirstValue);
        spinner.setAdapter(firstValueSpinnerAdapter);

        final EditText firstValueToConvert = (EditText) rootView.findViewById(R.id.editText1);
        final TextView result = (TextView) rootView.findViewById(R.id.editText2);

        Button convertButton = (Button) rootView.findViewById(R.id.button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conversion = spinner.getSelectedItem().toString();
                String value = firstValueToConvert.getText().toString();
                result.setText(timeWorker(value, conversion));
            }
        });

        return rootView;
    }

    public static String timeWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }

        switch (convertation) {
            case "Seconds to Minutes":
                result = Double.toString(value / 60);
                break;
            case "Seconds to Hour":
                result = Double.toString(value / 3600);
                break;
            case "Seconds to Day":
                result = Double.toString(value / 86400);
                break;
            case "Seconds to Week":
                result = Double.toString(value / 604800);
                break;
            case "Seconds to Month":
                result = Double.toString(value / 2419200);
                break;
            case "Seconds to Astronomical Year":
                result = Double.toString(value / 31536000);
                break;
            case "Seconds to Third":
                result = Double.toString(value / 6000000);
                break;
            case "Minutes to Seconds":
                result = Double.toString(value * 60);
                break;
            case "Hour to Seconds":
                result = Double.toString(value * 3600);
                break;
            case "Day to Seconds":
                result = Double.toString(value * 86400);
                break;
            case "Week to Seconds":
                result = Double.toString(value * 604800);
                break;
            case "Month to Seconds":
                result = Double.toString(value * 2419200);
                break;
            case "Astronomical Year to Seconds":
                result = Double.toString(value * 31536000);
                break;
            case "Third to Seconds":
                result = Double.toString(value * 60);
                break;
            default:
                result = "Choose convertation";
        }
        return result;

    }
}
