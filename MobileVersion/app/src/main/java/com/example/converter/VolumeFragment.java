package com.example.converter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class VolumeFragment extends Fragment {
    private String[] velichini = {"l to m^3", "l to gallon", "l to pint", "l to quart", "l to barrel", "l to cubic foot", "l to cubic inch", "m^3 to l", "gallon to l", "pint to l", "quart to l", "barrel to l", "cubic foot to l", "cubic inch to l"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.converter_screen, container, false);

        ArrayAdapter<String> firstValueSpinnerAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, velichini);
        firstValueSpinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinnerFirstValue);
        spinner.setAdapter(firstValueSpinnerAdapter);

        final EditText firstValueToConvert = (EditText) rootView.findViewById(R.id.editText1);
        final TextView result = (TextView) rootView.findViewById(R.id.editText2);

        Button convertButton = (Button) rootView.findViewById(R.id.button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conversion = spinner.getSelectedItem().toString();
                String value = firstValueToConvert.getText().toString();
                result.setText(volumeWorker(value, conversion));
            }
        });

        return rootView;
    }

    public static String volumeWorker(String input, String convertation) {
        double value = 0;
        String result;

        try {
            value = Double.parseDouble(input);
        } catch (Exception e) {
            result = "Enter only numbers!";
            return result;
        }
        switch (convertation) {
            case "l to m^3":
                result = Double.toString(value * 0.001);
                break;
            case "l to gallon":
                result = Double.toString(value * 0.2642);
                break;
            case "l to pint":
                result = Double.toString(value * 2.113);
                break;
            case "l to quart":
                result = Double.toString(value * 1.057);
                break;
            case "l to barrel":
                result = Double.toString(value * 0.00629);
                break;
            case "l to cubic foot":
                result = Double.toString(value * 0.0353146667);
                break;
            case "l to cubic inch":
                result = Double.toString(value * 61.0237441);
                break;
            case "m^3 to l":
                result = Double.toString(value / 0.001);
                break;
            case "gallon to l":
                result = Double.toString(value / 0.2642);
                break;
            case "pint to l":
                result = Double.toString(value / 2.113);
                break;
            case "quart to l":
                result = Double.toString(value / 1.057);
                break;
            case "barrel to l":
                result = Double.toString(value / 0.00629);
                break;
            case "cubic foot to l":
                result = Double.toString(value / 0.0353146667);
                break;
            case "cubic inch to l":
                result = Double.toString(value / 61.0237441);
                break;
            default:
                result = "Choose convertation";
        }
        return result;

    }
}
