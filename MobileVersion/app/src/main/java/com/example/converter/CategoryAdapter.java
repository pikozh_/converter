package com.example.converter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CategoryAdapter extends FragmentPagerAdapter {
    private Context mContext;

    /**
     * Create a new {@link CategoryAdapter} object.
     *
     * @param context is the context of the app
     * @param fm      is the fragment manager that will keep each fragment's state in the adapter
     *                across swipes.
     */
    public CategoryAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    /**
     * Return the {@link Fragment} that should be displayed for the given page number.
     */
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new TemperatureFragment();
        } else if (position == 1) {
            return new LengthFragment();
        } else if (position == 2) {
            return new TimeFragment();
        } else if (position == 3) {
            return new VolumeFragment();
        } else {
            return new WeightFragment();
        }
    }

    /**
     * Return the total number of pages.
     */
    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return "Temperature";
        } else if (position == 1) {
            return "Length";
        } else if (position == 2) {
            return "Time";
        } else if (position == 3) {
            return "Volume";
        } else {
            return "Weight";
        }
    }
}
